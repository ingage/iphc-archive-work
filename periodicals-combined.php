<?php
ini_set('display_errors',1);  error_reporting(E_ALL);
ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Title",
	2 => "Volume",
	3 => "Number",
	4 => "Date",
	5 => "Editor",
	6 => "Publisher",
	7 => "Location",
	8 => "Description",
	9 => "Religious Background"
);

$json = array();

$newsletters = fopen(dirname( __FILE__ ) . '/files/Newsletters-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($newsletters)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

		$result['source'] = "newsletters";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 5){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;


	// pretty_print_r($line);


 }

 fclose($newsletters);


$index = array(
	0 => "ItemType",
	1 => "Title",
	2 => "Volume",
	3 => "Number",
	4 => "Date",
	5 => "Editor",
	6 => "Publisher",
	7 => "Location",
	8 => "Description",
	9 => "Religious Background"
);

$phnewsletters = fopen(dirname( __FILE__ ) . '/files/Pentecostal Newsletters-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($phnewsletters)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

	$result['source'] = "phnewsletters";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);



		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 5){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = utf8_encode($v);

	}

	$json[] = $result;


	// echo '<pre>';
	// print_r($line);
	// echo '</pre>';

 }

fclose($phnewsletters);

// echo '<pre>';
// print_r($json);
// echo '</pre>';

die(json_encode($json)); 