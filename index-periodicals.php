<!doctype html>
<html class="no-js" lang="" ng-app="authorsApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>IPHC Archives - All Periodicals</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bower_components/foundation/css/normalize.css">
        <link rel="stylesheet" href="bower_components/foundation/css/foundation.css">

         <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
         <script src="bower_components/angular-foundation/mm-foundation-tpls.min.js"></script>


    <script>

      var authorsApp = angular.module('authorsApp', ['mm.foundation']);


      authorsApp.controller('AuthorsCtrl', function ($scope, $http, $modal){

        $http.get('periodicals-combined.php').success(function(data) {
          $scope.authors = data;
        });

        $scope.sortField = '';

           $scope.open = function (item) {

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: 'ModalInstanceCtrl',
              resolve: {
                authors: function () {
                  return $scope.authors;
                },
                item: function(){
                  return item;
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
             // $log.info('Modal dismissed at: ' + new Date());
            });
          };

      }); // AuthorsCtrl

      angular.module('authorsApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, authors, item) {

        $scope.authors = authors;
        $scope.item = item;

        $scope.selected = $scope.item;

        $scope.ok = function () {
          $modalInstance.close($scope.selected.author);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      });
    </script>



       </head>
<body>
<?php include("navigation.php");?>
<div class="row">

<h1>IPHC Archives - All Periodicals</h1>

</div>
<div class="row">
  <div class="large-12 columns">
        <input ng-model="search.$" type="text" placeholder="Search Everything" />
  </div>
</div>
<div class="row">
  <div class="large-3 columns">
    <input ng-model="search.volume" type="text" placeholder="Search Volume" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.title" type="text" placeholder="Search Titles" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.publisher" type="text" placeholder="Search Publisher" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.date" type="text" placeholder="Search Date" />
    </div>
</div>
</div>
<div ng-controller="AuthorsCtrl" class="row">

  <table>
    <thead>
      <tr>
        <th>Details</th>
        <th><a href="" ng-click="sortField = 'itemtype'">ItemType</a></th>
        <th>Source</th> 

        <th><a href="" ng-click="sortField = 'volume'">Volume</a></th>
        <th><a href="" ng-click="sortField = 'title'">Title</a></th> 
        <th><a href="" ng-click="sortField = 'publisher'">Publisher</a></th> 
        <th><a href="" ng-click="sortField = 'date'">Date</a></th> 
<!--        <th>Use</th> 
        <th>Type</th> 
         <th>Description</th> 
        <th>Subjects</th> 
        <th>Orig Pub Location</th> 
        <th>Orig Publisher</th> 
        <th>Orig Date</th>  
        <th>ISBN/ISSN</th>-->
      </tr>
      </thead>
      <tbody>
        <tr ng-repeat="author in authors | filter:search:strict | orderBy:sortField">
          <td><button class="button small" ng-click="open(author)">Details</button></td>
          <td>{{(author.itemtype) || "None specified"}}</td>
          <td>{{(author.source) || "None specified"}}</td>

          <td>{{(author.volume) || "None specified"}}</td>
          <td>{{(author.title) || "None specified"}}</td>
          <td>{{(author.publisher) || "None specified"}}</td>
          <td>{{(author.date) || "None specified"}}</td>
<!--          <td>{{author.use}}</td>
          <td>{{author.type}}</td>
           <td>{{author.description}}</td>
          <td>{{author.subjects}}</td>
           <td>{{author.origpublocation}}</td>
          <td>{{author.origpublisher}}</td>
          <td>{{author.origdate}}</td>
          <td>{{author.isbn}}</td>-->
        </tr>
      </tbody>
    </table>

     <script type="text/ng-template" id="myModalContent.html">
        <h3>{{ selected.title }}</h3>
        <p>Volume: <b>{{ (selected.volume) || "None specified" }}</b></p>
        <p>Number: <b>{{ (selected.number) || "None specified" }}</b></p>
        <p>Date: <b>{{ (selected.date) || "None specified" }}</b></p>

        <p>Editor: <b>{{ (selected.editor) || "None specified" }}</b></p>
        <p>Publisher: <b>{{ (selected.publisher) || "None specified" }}</b></p>
        <p>Location: <b>{{ (selected.location) || "None specified" }}</b></p>
      
        <p>Religious Background: <b>{{ (selected.religiousbackground) || "None specified" }}</b></p>

        <p>Description:<br /><b>{{ (selected.description) || "None specified" }}</b></p>

        <button class="button" ng-click="ok()">OK</button>
        <a class="close-reveal-modal" ng-click="cancel()">&#215;</a>
    </script>
</div>

</body>