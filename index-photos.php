<!doctype html>
<html class="no-js" lang="" ng-app="authorsApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>IPHC Archives - All Photos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bower_components/foundation/css/normalize.css">
        <link rel="stylesheet" href="bower_components/foundation/css/foundation.css">

         <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
         <script src="bower_components/angular-foundation/mm-foundation-tpls.min.js"></script>


    <script>

      var authorsApp = angular.module('authorsApp', ['mm.foundation']);


      authorsApp.controller('AuthorsCtrl', function ($scope, $http, $modal){

        $http.get('photos.php').success(function(data) {
          $scope.authors = data;
        });

        $scope.sortField = '';

           $scope.open = function (item) {

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: 'ModalInstanceCtrl',
              resolve: {
                authors: function () {
                  return $scope.authors;
                },
                item: function(){
                  return item;
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
             // $log.info('Modal dismissed at: ' + new Date());
            });
          };

      }); // AuthorsCtrl

      angular.module('authorsApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, authors, item) {

        $scope.authors = authors;
        $scope.item = item;

        $scope.selected = $scope.item;
        $scope.ok = function () {
          $modalInstance.close($scope.selected.author);
        };

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      });
    </script>



       </head>
<body>
<?php include("navigation.php");?>
<div class="row">

<h1>IPHC Archives - All Photos</h1>

</div>
<div class="row">
  <div class="large-12 columns">
        <input ng-model="search.$" type="text" placeholder="Search Everything" />
  </div>
</div>
<div class="row">
  <div class="large-3 columns">
    <input ng-model="search.participants" type="text" placeholder="Search Participants" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.conference" type="text" placeholder="Search Conference" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.event" type="text" placeholder="Search Events" />
  </div>
  <div class="large-3 columns">
    <input ng-model="search.photodate" type="text" placeholder="Search Dates" />
    </div>
</div>
</div>
<div ng-controller="AuthorsCtrl" class="row">

  <table>
    <thead>
      <tr>
        <th>Details</th>
        <th><a href="" ng-click="sortField = 'itemtype'">ItemType</a></th>
        <th>Source</th> 

        <th><a href="" ng-click="sortField = 'participants'">Participants</a></th>
        <th><a href="" ng-click="sortField = 'conference'">Conference</a></th> 
        <th><a href="" ng-click="sortField = 'event'">Event</a></th> 
        <th><a href="" ng-click="sortField = 'photodate'">Photo Date</a></th> 
<!--        <th>Use</th> 
        <th>Type</th> 
         <th>Description</th> 
        <th>Subjects</th> 
        <th>Orig Pub Location</th> 
        <th>Orig Publisher</th> 
        <th>Orig Date</th>  
        <th>ISBN/ISSN</th>-->
      </tr>
      </thead>
      <tbody>
        <tr ng-repeat="author in authors | filter:search:strict | orderBy:sortField">
          <td><button class="button small" ng-click="open(author)">Details</button></td>
          <td>{{(author.itemtype) || "None specified"}}</td>
          <td>{{(author.source) || "None specified"}}</td>

          <td>{{(author.participants) || "None specified"}}</td>
          <td>{{(author.conference) || "None specified"}}</td>
          <td>{{(author.event) || "None specified"}}</td>
          <td>{{(author.photodate) || "None specified"}}</td>
<!--          <td>{{author.use}}</td>
          <td>{{author.type}}</td>
           <td>{{author.description}}</td>
          <td>{{author.subjects}}</td>
           <td>{{author.origpublocation}}</td>
          <td>{{author.origpublisher}}</td>
          <td>{{author.origdate}}</td>
          <td>{{author.isbn}}</td>-->
        </tr>
      </tbody>
    </table>

     <script type="text/ng-template" id="myModalContent.html">
        <h3>Photo Information</h3>
        <p>Photo Date: <b>{{ (selected.photodate) || "None specified" }}</b></p>
        <p>Location: <b>{{ (selected.location) || "None specified" }}</b></p>
        <p>Conference: <b>{{ (selected.conference) || "None specified" }}</b></p>


        <p>Event: <b>{{ (selected.event) || "None specified" }}</b></p>

        <p>Participants: <b>{{ (selected.participants) || "None specified" }}</b></p>
        
        <p>Donor: <b>{{ (selected.donor) || "None specified" }}</b></p>

        <p>Description:<br /><b>{{ (selected.description) || "None specified" }}</b></p>

        <button class="button" ng-click="ok()">OK</button>
        <a class="close-reveal-modal" ng-click="cancel()">&#215;</a>
    </script>
</div>

</body>