<?php

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title",
	3 => "Pub Location",
	4 => "Publisher",
	5 => "Date",
	6 => "Edition",
	9 => "Location",
	17 => "Use",
	19 => "Type",
	23 => "Description",
	24 => "Subjects",
	28 => "Orig Pub Location",
	29 => "Orig Publisher",
	30 => "Orig Date",
	47 => "ISBN"
);

$json = array();

$authors = fopen(dirname( __FILE__ ) . '/files/Authors-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($authors)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

		$result['source'] = "authors";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 24){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;


	// pretty_print_r($line);


 }

 fclose($authors);


 $index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title",
	3 => "Edition",
	4 => "Publisher",
	5 => "Pub Location",
	6 => "Date",
	7 => "Location",
	8 => "Use",
	9 => "Subjects",
	10 => "Description",
	11 => "Comments",
	/*28 => "Orig Pub Location",
	29 => "Orig Publisher",
	30 => "Orig Date",
	47 => "ISBN" */
);


$books = fopen(dirname( __FILE__ ) . '/files/Books-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($books)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

	$result['source'] = "books";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 9){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;


	// pretty_print_r($line);


 }

 fclose($books);

 $index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title",
	3 => "Pub Location",
	4 => "Publisher",
	5 => "Date",
	6 => "Edition",
	7 => "Call Number",
	9 => "Location",
	17 => "Type",
	19 => "Use",
	23 => "Description",
	24 => "Subjects",
	28 => "Orig Pub Location",
	29 => "Orig Publisher",
	30 => "Orig Date" ,
	47 => "ISBN/ISSN" ,
	49 => "Medium"
);


$phauthors = fopen(dirname( __FILE__ ) . '/files/PH Authors-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($phauthors)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

		$result['source'] = "phauthors";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 24){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($phauthors);

die(json_encode($json)); 