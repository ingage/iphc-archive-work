<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title",
	3 => "Pub Location",
	4 => "Publisher",
	5 => "Date",
	6 => "Edition",
	7 => "Call Number",
	9 => "Location",
	17 => "Type",
	19 => "Use",
	23 => "Description",
	24 => "Subjects",
	28 => "Orig Pub Location",
	29 => "Orig Publisher",
	30 => "Orig Date" ,
	47 => "ISBN/ISSN" ,
	49 => "Medium"
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/PH Authors-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($csvfile)) !== FALSE) {

	$line = array_filter($line);

	$result = array();


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 24){
			$v = str_replace("//", ", ", $v);
		}


		$result[$name] = $v;

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 