<!doctype html>
<html class="no-js" lang="" ng-app="authorsApp">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>IPHC Archives - All Books</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bower_components/foundation/css/normalize.css">
        <link rel="stylesheet" href="bower_components/foundation/css/foundation.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.1/angular.min.js"></script>
        <script src="bower_components/angular-foundation/mm-foundation-tpls.min.js"></script>
        
        <script>
            
            var authorsApp = angular.module('authorsApp', ['mm.foundation']);

            authorsApp.controller('AuthorsCtrl', function ($scope, $http, $modal){

                $scope.start=0;
                $scope.end=10;

                $scope.next=function(){
                  $scope.end=parseInt($scope.end)+10;
                  $scope.start=parseInt($scope.start)+10;
                };

                $scope.previous=function(){
                  $scope.start=parseInt($scope.start-10);
                  $scope.end=parseInt($scope.end-10);
                };

                

              
              /*====================================================
                =   Function to Call all Books on load of page    =
              ====================================================*/

              
              $http.get('books-combined.php').success(function(data) {
                  $scope.authors = data;
                  $scope.showBook = true;
                  $scope.book = true;
              });
        

              /*====================================================
                =         Function to Call all Books       =
              ====================================================*/

              $scope.callBooks = function(book){
                if(book){
                  $scope.showBook = true;
                  $http.get('books-combined.php').success(function(data) {
                    $scope.authors = data;
                  });
                }else{
                  $scope.showBook = false;
                }

                
              };

             
              /*====================================================
                =         Function to Call all Multimedia        =
              ====================================================*/
                
              $scope.callMultimedias = function(multimedia){

                if(multimedia){
                    $scope.showMedia = true;
                    $http.get('multimedia.php').success(function(data) {
                      $scope.multimedias = data;
                    });
                }else{
                    $scope.showMedia = false;
                }
              };


              /*====================================================
                =         Function to Call all Periodicals        =
              ====================================================*/

              $scope.callPeriodicals=function(periodical){
                if(periodical){
                    $scope.showPeriodicals=true; 
                    $http.get('periodicals-combined.php').success(function(data) {
                      $scope.periodicals = data;
                    });
                }else{
                    $scope.showPeriodicals = false;
                }
              };


              /*====================================================
                =         Function to Call all Photos         =
              ====================================================*/


              $scope.callPhotos=function(photo){
                if(photo ){
                    $scope.showPhotos=true; 
                    $http.get('photos.php').success(function(data) {
                      $scope.photos = data;
                    });
                }else{
                    $scope.showPhotos = false;
                }

              };


              /*====================================================
                =         Function to Call all Donors         =
              ====================================================*/

              $scope.callDonors=function(donor){
                if(donor){
                    $scope.showDonors=true;
                    $http.get('Donors.php').success(function(data) {
                      $scope.donors = data;
                    });
                }else{
                    $scope.showDonors = false;
                }

              };

              /*====================================================
                =         Function to Call Minutes         =
              ====================================================*/

              $scope.callMinutes=function(minute){
                if(minute){
                    $scope.showMinutes=true;
                    $http.get('minutes.php').success(function(data) {
                      $scope.minutes = data;
                    });
                }else{
                    $scope.showMinutes = false;
                }

              };
        

              $scope.sortField = '';

              $scope.open = function (item) {

                var modalInstance = $modal.open({
                  templateUrl: 'myModalContent.html',
                  controller: 'ModalInstanceCtrl',
                  resolve: {
                    authors: function () {
                      return $scope.authors;
                    },
                    item: function(){
                      return item;
                    }
                  }
                });

                modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                }, function () {
                 // $log.info('Modal dismissed at: ' + new Date());
                });
              };


          

            }); // AuthorsCtrl

            angular.module('authorsApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, authors, item) {

              $scope.authors = authors;
              $scope.item = item;


              $scope.selected = $scope.item;
              $scope.ok = function () {
                $modalInstance.close($scope.selected.author);
              };

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
            });

            authorsApp.filter('slice', function() {
              return function(arr, start, end) {
                  if(arr){
                      return arr.slice(start, end);
                  }
              };
            });

        </script>
    </head>

    <body>
        <?php include("navigation.php");?>
        <div class="row">
            <h1>IPHC Archives</h1>
        </div>

        <div class="row">
          <div class="large-12 columns">
                <input ng-model="search.$" type="text" placeholder="Search Everything" />
          </div>
        </div>
        <div class="row">
          <div class="large-3 columns">
            <input ng-model="search.authors" type="text" placeholder="Search Authors" />
          </div>
          <div class="large-3 columns">
            <input ng-model="search.title" type="text" placeholder="Search Titles" />
          </div>
          <div class="large-3 columns">
            <input ng-model="search.publisher" type="text" placeholder="Search Publisher" />
          </div>
          <div class="large-3 columns">
            <input ng-model="search.subjects" type="text" placeholder="Search Subjects" />
          </div>
        </div>

        <div ng-controller="AuthorsCtrl" class="row">


            <div class="row">
              <div class="button small" ng-click="previous()">
                  Previous
              </div>
              <div class="button small" ng-click="next()">
                  Next
              </div>
            </div>
            
            <div class="large-2 columns">
                <input type="checkbox" ng-model="book" ng-change="callBooks(book)">&nbsp;<b>Books</b> 
            </div>
            <div class="large-2 columns">
                <input  type="checkbox" ng-model="multimedia" ng-change="callMultimedias(multimedia)">&nbsp;<b>Multimedia</b>
            </div>
            <div class="large-2 columns">
                <input  type="checkbox" ng-model="periodical" ng-change="callPeriodicals(periodical)">&nbsp;<b>Periodicals</b>
            </div>
            <div class="large-2 columns">
                <input type="checkbox" ng-model="photo" ng-change="callPhotos(photo)">&nbsp;<b>Photos</b> 
            </div>
            <div class="large-2 columns">
                <input type="checkbox" ng-model="donor" ng-change="callDonors(donor)">&nbsp;<b>Donors</b>
            </div>
            <div class="large-2 columns">
                <input type="checkbox" ng-model="minute" ng-change="callMinutes(minute)">&nbsp;<b>Minutes</b>
            </div>
            
            <table>
                <thead>
                    <tr>
                      <th>Details</th>
                      <th><a href="" ng-click="sortField = 'itemtype'">ItemType</a></th>
                      <th>Source</th> 
                      <th ng-if="showPeriodicals"><a href="" ng-click="sortField = 'volume'">Volume</a></th>
                      <th ng-if="showBook || showMedia"><a href="" ng-click="sortField = 'authors'">Authors</a></th>
                      <th ng-if="showBook || showDonors || showMedia || showPeriodicals"><a href="" ng-click="sortField = 'title'">Title</a></th> 
                      <th ng-if="showBook || showDonors || showMedia || showPeriodicals"><a href="" ng-click="sortField = 'publisher'">Publisher</a></th> 
                      <th ng-if="showBook || showDonors || showMedia || showPeriodicals"><a href="" ng-click="sortField = 'date'">Date</a></th> 
                      <th ng-if="showPhotos"><a href="" ng-click="sortField = 'participants'">Participants</a></th>
                      <th ng-if="showPhotos"><a href="" ng-click="sortField = 'conference'">Conference</a></th> 
                      <th ng-if="showPhotos"><a href="" ng-click="sortField = 'event'">Event</a></th> 
                      <th ng-if="showPhotos"><a href="" ng-click="sortField = 'photodate'">Photo Date</a></th> 
                      <th ng-if="showDonors"><a href="" ng-click="sortField = 'authors'">Donors</a></th>
                      <th ng-if="showMinutes"><a href="" ng-click="sortField = 'year'">Year</a></th>
                      <th ng-if="showMinutes"><a href="" ng-click="sortField = 'session'">Session</a></th> 
                      <th ng-if="showMinutes"><a href="" ng-click="sortField = 'conference'">Conference</a></th> 
                      <th ng-if="showMinutes"><a href="" ng-click="sortField = 'donor'">Donor</a></th> 
                    </tr>
                </thead>
                
                <tbody>

                    <tr ng-show="showBook" ng-repeat="author in authors |  slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(author)">Details</button></td>
                      <td>{{(author.itemtype) || "None specified"}}</td>
                      <td>{{(author.source) || "None specified"}}</td>
                      <td ng-if="showPeriodicals">{{"None specified"}}</td>
                      <td>{{(author.authors) || "None specified"}}</td>
                      <td>{{(author.title) || "None specified"}}</td>
                      <td>{{(author.publisher) || "None specified"}}</td>
                      <td>{{(author.date) || "None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showDonors">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                    </tr>

                    <tr ng-show="showMedia" ng-repeat="multimedia in multimedias | slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(multimedia)">Details</button></td>
                      <td>{{(multimedia.itemtype) || "None specified"}}</td>
                      <td>{{(multimedia.source) || "None specified"}}</td>
                      <!-- <td ng-if="showPeriodicals">{{"None specified"}}</td> -->
                      <td>{{(multimedia.authors) || "None specified"}}</td>
                      <td>{{(multimedia.title) || "None specified"}}</td>
                      <td>{{(multimedia.publisher) || "None specified"}}</td>
                      <td>{{(multimedia.date) || "None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showDonors">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                    </tr>

                    <tr ng-show="showPeriodicals" ng-repeat="periodical in periodicals| slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(periodical)">Details</button></td>
                      <td>{{(periodical.itemtype) || "None specified"}}</td>
                      <td>{{(periodical.source) || "None specified"}}</td>
                      <td>{{(periodical.volume) || "None specified"}}</td>
                      <td ng-if="showBook || showMedia">{{"None specified"}}</td>
                      <td>{{(periodical.title) || "None specified"}}</td>
                      <td>{{(periodical.publisher) || "None specified"}}</td>
                      <td>{{(periodical.date) || "None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showDonors">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                    </tr>

                    <tr ng-show="showPhotos" ng-repeat="photo in photos | slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(photo)">Details</button></td>
                      <td>{{(photo.itemtype) || "None specified"}}</td>
                      <td>{{(photo.source) || "None specified"}}</td>
                      <td ng-if="showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showMedia">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td>{{(photo.participants) || "None specified"}}</td>
                      <td>{{(photo.conference) || "None specified"}}</td>
                      <td>{{(photo.event) || "None specified"}}</td>
                      <td>{{(photo.photodate) || "None specified"}}</td>
                      <td ng-if="showDonors">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                    </tr>

                    <tr ng-show="showDonors" ng-repeat="donor in donors | slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(donor)">Details</button></td>
                      <td>{{(donor.itemtype) || "None specified"}}</td>
                      <td>{{(donor.source) || "None specified"}}</td>
                      <td ng-if="showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showMedia">{{"None specified"}}</td>
                      <td>{{(donor.title) || "None specified"}}</td>
                      <td>{{(donor.publisher) || "None specified"}}</td>
                      <td>{{(donor.date) || "None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td>{{(donor.donors) || "None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                      <td ng-if="showMinutes">{{"None specified"}}</td>
                    </tr>

                    <tr ng-show="showMinutes" ng-repeat="minute in minutes | slice:start:end | filter:search:strict | orderBy:sortField">
                      <td><button class="button small" ng-click="open(minute)">Details</button></td>
                      <td>{{(minute.itemtype) || "None specified"}}</td>
                      <td>{{(minute.source) || "None specified"}}</td>
                      <td ng-if="showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showMedia">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showBook || showDonors || showMedia || showPeriodicals">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showPhotos">{{"None specified"}}</td>
                      <td ng-if="showDonors">{{"None specified"}}</td>
                      <td>{{(minute.year) || "None specified"}}</td>
                      <td>{{(minute.session) || "None specified"}}</td>
                      <td>{{(minute.conference) || "None specified"}}</td>
                      <td>{{(minute.donor) || "None specified"}}</td>
                    </tr>

                </tbody>
            </table>
        
            <script type="text/ng-template" id="myModalContent.html">
                <h3>{{ selected.title }}</h3>
                <p>Volume: <b>{{ (selected.volume) || "None specified" }}</b></p>
                <p>Number: <b>{{ (selected.number) || "None specified" }}</b></p>
                <p>Date: <b>{{ (selected.date) || "None specified" }}</b></p>

                <p>Editor: <b>{{ (selected.editor) || "None specified" }}</b></p>
                <p>Publisher: <b>{{ (selected.publisher) || "None specified" }}</b></p>
                <p>Location: <b>{{ (selected.location) || "None specified" }}</b></p>
              
                <p>Religious Background: <b>{{ (selected.religiousbackground) || "None specified" }}</b></p>

                <p>Description:<br /><b>{{ (selected.description) || "None specified" }}</b></p>

                <button class="button" ng-click="ok()">OK</button>
                <a class="close-reveal-modal" ng-click="cancel()">&#215;</a>
            </script>
        </div>
    </body>
</html>