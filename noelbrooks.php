<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	//1 => "Title",
	2 => "Title", 
	3 => "Publisher", 
	4 => "Publisher Location", 
	5 => "Date", 
	6 => "Publisher", 
	7 => "Subjects", 
	8 => "Description", 
	9 => "Volume", 
	10 => "Issue", 
	11 => "Editors", 
	/*37 => "Performers", 
	38 => "Performer Role", 
	30 => "Orig Date", 
	47 => "ISBN" */
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/Noel Brooks-comma.txt', 'r');
$count = 0;
// $fp = file(dirname( __FILE__ ) . '/retailers.csv', FILE_SKIP_EMPTY_LINES);
// echo 'Lines: ' . (count($fp)-1) . "<br /><hr />";
while (($line = fgetcsv($csvfile)) !== FALSE) {
  // $line is an array of the csv elements
  // 
	$line = array_filter($line);

	$result = array();

	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 7){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 