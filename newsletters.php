<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Title",
	2 => "Volume", 
	3 => "Number", 
	4 => "Date", 
	5 => "Editor", 
	6 => "Publisher", 
	7 => "Location", 
	8 => "Description", 
	9 => "Religious Background", 
	/*24 => "Subjects", 
	27 => "Author Role", 
	37 => "Performers", 
	38 => "Performer Role", 
	30 => "Orig Date", 
	47 => "ISBN" */
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/newsletters-comma.txt', 'r');
$count = 0;
// $fp = file(dirname( __FILE__ ) . '/retailers.csv', FILE_SKIP_EMPTY_LINES);
// echo 'Lines: ' . (count($fp)-1) . "<br /><hr />";
while (($line = fgetcsv($csvfile)) !== FALSE) {
  // $line is an array of the csv elements
  // 
	$line = array_filter($line);

	$result = array();

	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 600){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 