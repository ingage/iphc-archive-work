<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Photo Date",
	2 => "Location", 
	3 => "Conference", 
	4 => "Event", 
	5 => "Participants", 
	6 => "Donor", 
	7 => "Description", 
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/Photos-comma.txt', 'r');
$count = 0;
// $fp = file(dirname( __FILE__ ) . '/retailers.csv', FILE_SKIP_EMPTY_LINES);
// echo 'Lines: ' . (count($fp)-1) . "<br /><hr />";
while (($line = fgetcsv($csvfile)) !== FALSE) {
  // $line is an array of the csv elements
  // 
	$line = array_filter($line);

	$result = array();

	$result["source"] = "photos";

	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 5){
			$v = str_replace("//", ", ", $v);
		}



		$result[$name] = utf8_encode($v);

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 