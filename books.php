<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title", 
	3 => "Edition", 
	4 => "Publisher", 
	5 => "Pub Location", 
	6 => "Date", 
	7 => "Location", 
	8 => "Use", 
	9 => "Subjects", 
	10 => "Description", 
	11 => "Comments", 
	/*28 => "Orig Pub Location", 
	29 => "Orig Publisher", 
	30 => "Orig Date", 
	47 => "ISBN" */
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/Books-comma.txt', 'r');
$count = 0;
while (($line = fgetcsv($csvfile)) !== FALSE) {

	$line = array_filter($line);

	$result = array();

	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 9){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;


	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 