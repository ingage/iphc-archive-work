<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Authors",
	2 => "Title", 
	3 => "Pub Location", 
	4 => "Publisher", 
	5 => "Date", 
	9 => "Location", 
	17 => "Use", 
	19 => "Type", 
	23 => "Description", 
	24 => "Subjects", 
	27 => "Author Role", 
	37 => "Performers", 
	38 => "Performer Role", 
	/*30 => "Orig Date", 
	47 => "ISBN" */
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/Multimedia-comma.txt', 'r');
$count = 0;
// $fp = file(dirname( __FILE__ ) . '/retailers.csv', FILE_SKIP_EMPTY_LINES);
// echo 'Lines: ' . (count($fp)-1) . "<br /><hr />";
while (($line = fgetcsv($csvfile)) !== FALSE) {
  // $line is an array of the csv elements
  // 
	$line = array_filter($line);

	$result = array();

	$result["source"] = "multimedia";

	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 24){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = $v;

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 