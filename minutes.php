<?php 

ini_set("auto_detect_line_endings", "1");

$index = array(
	0 => "ItemType",
	1 => "Year",
	2 => "Session", 
	3 => "Conference", 
	4 => "Church", 
	5 => "Convened", 
	6 => "Donor", 
	7 => "Location", 
	8 => "Comments", 
	/*17 => "Use", 
	23 => "Description", 
	24 => "Subjects", 
	27 => "Orig Pub Location", 
	29 => "Orig Publisher", 
	30 => "Orig Date", 
	47 => "ISBN" */
);

$json = array();

$csvfile = fopen(dirname( __FILE__ ) . '/files/Minutes-comma.txt', 'r');
$count = 0;
// $fp = file(dirname( __FILE__ ) . '/retailers.csv', FILE_SKIP_EMPTY_LINES);
// echo 'Lines: ' . (count($fp)-1) . "<br /><hr />";
while (($line = fgetcsv($csvfile)) !== FALSE) {
  // $line is an array of the csv elements
  // 
	$line = array_filter($line);

	$result = array();

		$result["source"] = "minutes";


	foreach($line as $key => $value){

		$name = $index[$key];
		$name = strtolower($name);
		$name = str_replace(" ", "", $name);


		if($value){
			$v = $value;
		} else {
			$v = "none specified";
		}

		if($key == 600){
			$v = str_replace("//", ", ", $v);
		}

		$result[$name] = utf8_encode($v);

	}

	$json[] = $result;

  
	// pretty_print_r($line);


 }

 fclose($csvfile);

die(json_encode($json)); 