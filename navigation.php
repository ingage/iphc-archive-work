<div class="contain-to-grid sticky">

<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="#">IPHC Archives</a></h1>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">
<!--       <li class="active"><a href="#">Right Button Active</a></li>
 -->      <li class="has-dropdown">
        <a href="#">Navigate</a>
        <ul class="dropdown">
          <li><a href="index-all.php">Books</a></li>
          <li><a href="index-periodicals.php">Periodicals</a></li>
          <li><a href="index-multimedia.php">Multimedia</a></li>
          <li><a href="index-photos.php">Photos</a></li>
          <li><a href="index-donors.php">Donors</a></li>
          <li><a href="index-minutes.php">Minutes</a></li>

        </ul>
      </li>
    </ul>

    <!-- Left Nav Section -->
    <ul class="left">
     <!--  <li><a href="#">Left Nav Button</a></li> -->
    </ul>
  </section>
</nav>
</div>